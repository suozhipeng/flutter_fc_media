import 'package:flutter/material.dart';
import 'dart:async';
// ignore: depend_on_referenced_packages
import 'package:flutter_vlc_player/flutter_vlc_player.dart';

import 'package:flutter/services.dart';
import 'package:flutter_fc_media/flutter_fc_media.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';
  String _nativeMsg = '';
  final _flutterFcMediaPlugin = FlutterFcMedia();

  late VlcPlayerController playController;
  late String nativeMsg = 'www.baidu.com';
  late String tmpRTSPPath = "";

  @override
  void initState() {
    super.initState();
    initPlatformState();

    playController = VlcPlayerController.network(
      nativeMsg,
      hwAcc: HwAcc.auto, // 硬件加速类型
      autoPlay: true, // 自动播放
      options: VlcPlayerOptions(),
    );

    FlutterFcMedia.onEventListener((value){
      if (!nativeMsg.contains("rtsp://")) {
        _nativeMsg = value as String;
        final tmpPath = value;
        // nativeMsg = 'rtsp://192.168.0.110:8554/$tmpPath';
        nativeMsg = 'rtsp://0.0.0.0:8554/$tmpPath';
        nativeMsg = 'rtsp://192.168.0.101:8554/$tmpPath'; // 当前手机的 ip 地址
        print("123监听到的数据是: $nativeMsg");
        playController.setMediaFromNetwork(nativeMsg, autoPlay: true);
        setState(() {});
      }
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initPlatformState() async {
    String platformVersion;
    // Platform messages may fail, so we use a try/catch PlatformException.
    // We also handle the message potentially returning null.
    try {
      platformVersion =
          await _flutterFcMediaPlugin.getPlatformVersion() ?? 'Unknown platform version';
    } on PlatformException {
      platformVersion = 'Failed to get platform version.';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _platformVersion = platformVersion;
    });
  }

  buildVlcPlayer()  {
    return VlcPlayer(
      controller: playController,
      aspectRatio: 16 / 9, // 视频宽高比
      placeholder: const Center(child: CircularProgressIndicator()),
    );
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              Text('Running on: $_platformVersion\n'),
              ElevatedButton(onPressed: (){
                FlutterFcMedia.actionForEventStart(rtpPort: 12345, rtspPort: 8554);
              }, child: const Text("开始监听原生消息")),
              Text('Native msg is: $_nativeMsg\n'),
              ElevatedButton(onPressed: (){
                FlutterFcMedia.actionForMediaReset();
                playController.dispose();
                setState(() {
                  nativeMsg = '';
                });
              }, child: const Text("取消监听")),
              Container(color: Colors.red,child: buildVlcPlayer(),),
              ElevatedButton(
                onPressed: () {
                  if (playController.value.isPlaying) {
                    playController.pause();
                  } else {
                    playController.play();
                  }
                },
                child: Icon(playController.value.isPlaying ? Icons.pause : Icons.play_arrow),
              )
            ],
          ),
        ),
      ),
    );
  }
}
// rtsp://192.168.0.110:8554/rtp/989E00D5
