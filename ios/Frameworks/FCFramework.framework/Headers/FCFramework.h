//
//  FCFramework.h
//  FCFramework
//
//  Created by Zhipeng on 2024/1/26.
//

#import <Foundation/Foundation.h>

//! Project version number for FCFramework.
FOUNDATION_EXPORT double FCFrameworkVersionNumber;

//! Project version string for FCFramework.
FOUNDATION_EXPORT const unsigned char FCFrameworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <FCFramework/PublicHeader.h>


#import "CameraMediakit.h"
#import "mk_mediakit.h"
