//
//  CameraMediakit.h
//  flutter_camera
//
//  Created by fky on 2024/1/24.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

typedef void (^CameraCallback)(NSString* rtspPath);

@interface CameraMediakit : NSObject


+ (instancetype)sharedInstance;

//// 添加其他实例方法和属性
//-(void)setup:(MyCallbackFunction)callback;

/** 通过监听 rtp 端口, 把数据重新包装, 并通过 rtsp 端口转发出来
 rtp: rtp监听端口(包括udp/tcp), 默认 10000
 rtsp: rtsp监听端口，推荐8554，[rtsp <1默认为 8554]
 */
-(void)handleDataFromRTPPort:(int)rtp toRtspPort:(int)rtsp resultCallback:(CameraCallback)callback;

// 重置所有服务
-(void)reset;

@end

NS_ASSUME_NONNULL_END
