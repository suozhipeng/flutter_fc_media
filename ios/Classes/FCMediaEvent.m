//
//  FCMediaEvent.m
//  flutter_fc_media
//
//  Created by fky on 2024/1/27.
//

#import "FCMediaEvent.h"
#import <FCFramework/FCFramework.h>


@implementation FCMediaEvent

+(void)handleMediaEventRTP:(int)rtpPort toRtspPort:(int)rtspPort resultCallback:(void(^)(NSString *rtspPath))callback{

    [CameraMediakit.sharedInstance handleDataFromRTPPort:rtpPort toRtspPort:rtspPort resultCallback:^(NSString * _Nonnull rtspPath) {
        if(callback) {
            callback(rtspPath);
        }
    }];
}

+(void) reset {
    [CameraMediakit.sharedInstance reset];
}
@end
