#import "FlutterFcMediaPlugin.h"

#import "FCMediaEvent.h"

@interface FlutterFcMediaPlugin ()<FlutterStreamHandler>

@property (nonatomic, strong) FlutterEventSink tmpEvent ;

@end

@implementation FlutterFcMediaPlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  FlutterMethodChannel* channel = [FlutterMethodChannel
      methodChannelWithName:@"flutter_fc_media"
            binaryMessenger:[registrar messenger]];
  FlutterFcMediaPlugin* instance = [[FlutterFcMediaPlugin alloc] init];
  [registrar addMethodCallDelegate:instance channel:channel];

    
  // 事件监听回调
  FlutterEventChannel *tmpEventChannel = [FlutterEventChannel eventChannelWithName:@"flutter_fc_media_event" binaryMessenger:[registrar messenger]];
  [tmpEventChannel setStreamHandler:instance];
}

- (void)handleMethodCall:(FlutterMethodCall*)call result:(FlutterResult)result {
  if ([@"getPlatformVersion" isEqualToString:call.method]) {
    result([@"iOS " stringByAppendingString:[[UIDevice currentDevice] systemVersion]]);
  } else if ([@"startListenEvent" isEqualToString:call.method]) {

      NSDictionary *info = call.arguments;
      int tmpRTP  = [info[@"rtp"] intValue];
      int tmpRTSP = [info[@"rtsp"] intValue];
      // 开启监听
      [self startListenEventRTP:tmpRTP toRtspPort:tmpRTSP];
      result(nil);
  } else if([@"handleForMediaResetAll" isEqualToString:call.method]){
      [FCMediaEvent reset];
      result(nil);
  }
  else {
    result(FlutterMethodNotImplemented);
  }
}


-(void)startListenEventRTP:(int)rtpPort toRtspPort:(int)rtspPort {

    [FCMediaEvent handleMediaEventRTP:rtpPort toRtspPort:rtspPort resultCallback:^(NSString * _Nonnull rtspPath) {
//        NSLog(@"当前地址是%@", rtspPath);
        [self addEvent:rtspPath];
    }];
}

-(void) addEvent: (NSObject *) event {
  if (self.tmpEvent != nil) {
    self.tmpEvent(event);
  }
}

- (FlutterError * _Nullable)onCancelWithArguments:(id _Nullable)arguments {
  self.tmpEvent = nil;
  return  nil;
}

- (FlutterError * _Nullable)onListenWithArguments:(id _Nullable)arguments eventSink:(nonnull FlutterEventSink)events {
  self.tmpEvent = events;
//  [self startListenEvent];

  return  nil;
}


@end
