//
//  FCMediaEvent.h
//  flutter_fc_media
//
//  Created by fky on 2024/1/27.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface FCMediaEvent : NSObject

/// 回调 Media
/// - Parameter callback: 回传获取到的地址
+(void)handleMediaEventRTP:(int)rtpPort toRtspPort:(int)rtspPort resultCallback:(void(^)(NSString *rtspPath))callback;

/// 重置所有服务
+(void) reset;

@end

NS_ASSUME_NONNULL_END
