#
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html.
# Run `pod lib lint flutter_fc_media.podspec` to validate before publishing.
#
Pod::Spec.new do |s|
  s.name             = 'flutter_fc_media'
  s.version          = '0.0.2'
  s.summary          = 'Flutter 解析插件.'
  s.description      = <<-DESC
视频解析插件 Flutter 版本, 依赖 ZLMediaKit, RTP-->RTSP
                       DESC
  s.homepage         = 'http://example.com'
  s.license          = { :file => '../LICENSE' }
  s.author           = { 'Your Company' => 'email@example.com' }
  s.source           = { :path => '.' }
  s.source_files = 'Classes/**/*'
  s.public_header_files = 'Classes/**/*.h'
  s.vendored_frameworks = 'Frameworks/FCFramework.framework'
  s.dependency 'Flutter'
  s.platform = :ios, '11.0'
  s.static_framework = true

  # Flutter.framework does not contain a i386 slice.
  s.pod_target_xcconfig = { 'DEFINES_MODULE' => 'YES', 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'i386' }
end
