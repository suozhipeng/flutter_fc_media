# flutter_fc_media

A new Flutter project.

## Getting Started

This project is a starting point for a Flutter
[plug-in package](https://flutter.dev/developing-packages/),
a specialized package that includes platform-specific implementation code for
Android and/or iOS.

For help getting started with Flutter development, view the
[online documentation](https://flutter.dev/docs), which offers tutorials,
samples, guidance on mobile development, and a full API reference.

- iOS & Android

```dart
  FlutterFcMedia.onEventListener((value){
      _nativeMsg = value as String;
      print("监听到的数据是: $_nativeMsg");
      setState(() {});
  });

  ElevatedButton(onPressed: (){
    FlutterFcMedia.actionForEventStart(rtpPort: 12345, rtspPort: 8554);
  }, child: Text("开始监听"))

  ElevatedButton(onPressed: (){
    FlutterFcMedia.actionForMediaReset();
  }, child: Text("移除对象"))
```

**注意**

- Flutter 打包 apk, debug版本正常、release版本Crash问题
  `flutter build apk --release`
>
```vim
JNI DETECTED ERROR IN APPLICATION: java_class == null
in call to GetFieldID
from java.lang.String java.lang.Runtime.nativeLoad(java.lang.String, java.lang.ClassLoader, java.lang.Class)
Runtime aborting…
Dumping all threads without mutator lock held
```
R8压缩, Flutter 中 release 版本是默认开启R8压缩的
然后就在`build.gradle`中修改配置
```vim
buildTypes {
    release {
        minifyEnabled false
        shrinkResources false
        signingConfig signingConfigs.release
    }
}
```
