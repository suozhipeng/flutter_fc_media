import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_fc_media/flutter_fc_media.dart';
import 'package:flutter_fc_media/flutter_fc_media_platform_interface.dart';
import 'package:flutter_fc_media/flutter_fc_media_method_channel.dart';
import 'package:plugin_platform_interface/plugin_platform_interface.dart';

class MockFlutterFcMediaPlatform
    with MockPlatformInterfaceMixin
    implements FlutterFcMediaPlatform {

  @override
  Future<String?> getPlatformVersion() => Future.value('42');
}

void main() {
  final FlutterFcMediaPlatform initialPlatform = FlutterFcMediaPlatform.instance;

  test('$MethodChannelFlutterFcMedia is the default instance', () {
    expect(initialPlatform, isInstanceOf<MethodChannelFlutterFcMedia>());
  });

  test('getPlatformVersion', () async {
    FlutterFcMedia flutterFcMediaPlugin = FlutterFcMedia();
    MockFlutterFcMediaPlatform fakePlatform = MockFlutterFcMediaPlatform();
    FlutterFcMediaPlatform.instance = fakePlatform;

    expect(await flutterFcMediaPlugin.getPlatformVersion(), '42');
  });
}
