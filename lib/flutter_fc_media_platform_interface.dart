import 'package:plugin_platform_interface/plugin_platform_interface.dart';

import 'flutter_fc_media_method_channel.dart';

abstract class FlutterFcMediaPlatform extends PlatformInterface {
  /// Constructs a FlutterFcMediaPlatform.
  FlutterFcMediaPlatform() : super(token: _token);

  static final Object _token = Object();

  static FlutterFcMediaPlatform _instance = MethodChannelFlutterFcMedia();

  /// The default instance of [FlutterFcMediaPlatform] to use.
  ///
  /// Defaults to [MethodChannelFlutterFcMedia].
  static FlutterFcMediaPlatform get instance => _instance;

  /// Platform-specific implementations should set this with their own
  /// platform-specific class that extends [FlutterFcMediaPlatform] when
  /// they register themselves.
  static set instance(FlutterFcMediaPlatform instance) {
    PlatformInterface.verifyToken(instance, _token);
    _instance = instance;
  }

  Future<String?> getPlatformVersion() {
    throw UnimplementedError('platformVersion() has not been implemented.');
  }
}
