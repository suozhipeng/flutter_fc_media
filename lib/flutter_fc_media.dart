
import 'dart:async';

import 'package:flutter/services.dart';

class FlutterFcMedia {

  static const _methodChannel = MethodChannel('flutter_fc_media');

  static const _eventChannel = EventChannel('flutter_fc_media_event');

  static StreamSubscription? _eventSubscription;

  /// 获取固件版本
  Future<String?> getPlatformVersion() async {
    final version = await _methodChannel.invokeMethod<String>('getPlatformVersion');
    print("当前获取到的 version 是: $version");
    return version;
  }

  /// Flutter --> 原生 (传递数据)
  static Future<void> actionForNativeView() async {
    await _methodChannel.invokeMethod('actionFlutterToNative', {"title":"Flutter传递来的数据"});
  }

  static Future<void> actionForEventStart({int rtpPort = 0, int rtspPort = 8554}) async {
    final tmpInfo = {'rtp':rtpPort, 'rtsp':rtspPort};
    print("开始监听: $tmpInfo, rtp端口是: $rtpPort");
    await _methodChannel.invokeMethod("startListenEvent", tmpInfo);
  }

  static Future<void>actionForMediaReset() async {
    print("关闭当前监听");
    await _methodChannel.invokeMethod("handleForMediaResetAll");
    await _eventSubscription?.cancel();
  }

  ///事件回调
  ///@params onData 事件回调
  static Future<void> onEventListener(Function callback) async {
    _eventSubscription = _eventChannel.receiveBroadcastStream().listen((data) {
      print("监听到的数据是: $data");
      callback(data);
    });
  }
}
