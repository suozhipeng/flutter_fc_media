import 'package:flutter/foundation.dart';
import 'package:flutter/services.dart';

import 'flutter_fc_media_platform_interface.dart';

/// An implementation of [FlutterFcMediaPlatform] that uses method channels.
class MethodChannelFlutterFcMedia extends FlutterFcMediaPlatform {
  /// The method channel used to interact with the native platform.
  @visibleForTesting
  final methodChannel = const MethodChannel('flutter_fc_media');

  @override
  Future<String?> getPlatformVersion() async {
    final version = await methodChannel.invokeMethod<String>('getPlatformVersion');
    return version;
  }
}
