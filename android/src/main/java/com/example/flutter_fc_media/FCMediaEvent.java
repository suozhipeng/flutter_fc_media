package com.example.flutter_fc_media;


import android.os.Handler;
import android.os.Looper;

import com.zlmediakit.jni.ZLMediaKit;


public class FCMediaEvent {

    // 定义一个接口作为回调
    public interface Callback {
        void onResult(String rtspPath);
    }

    public static void handleMediaEventRTP(int rtpPort, int rtspPort, Callback callback) {
        ZLMediaKit.setupServer("", rtpPort);
        ZLMediaKit.startServer(rtpPort);
        ZLMediaKit.setupMediaListener(new ZLMediaKit.FCMediaInfoCallback() {
            @Override
            public void mediaInfo(int code, String rtspPath) {
                //rtsp://0.0.0.0:8554/rtp/ED216915
                //System.out.println("mediaInfo: 当前路径是: " + rtspPath);
                // 创建一个在主线程上执行的 Handler
                Handler mainThreadHandler = new Handler(Looper.getMainLooper());
                // 在这里处理结果并将其传递到主线程
                mainThreadHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        // 在主线程上执行的代码
                        System.out.println("Callback xxxxxxxxxxxxxxxxxxxxxxx " + rtspPath + " xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx");
                        callback.onResult("rtp/"+rtspPath);
                    }
                });
            }
        });
    }

    public static void reset() {
        ZLMediaKit.stopServer(0);
    }
}