package com.example.flutter_fc_media;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import androidx.annotation.NonNull;

import io.flutter.embedding.engine.plugins.FlutterPlugin;
import io.flutter.plugin.common.BinaryMessenger;
import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.EventChannel;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;

import android.os.Handler;
import android.os.Looper;
import com.zlmediakit.jni.ZLMediaKit;

import java.util.Map;

import javax.security.auth.callback.Callback;

/** FlutterFcMediaPlugin */
public class FlutterFcMediaPlugin implements FlutterPlugin, MethodCallHandler {
  /// The MethodChannel that will the communication between Flutter and native Android
  ///
  /// This local reference serves to register the plugin with the Flutter Engine and unregister it
  /// when the Flutter Engine is detached from the Activity
  private MethodChannel channel;
  // 事件通道名称
  public static final String eventChannelName = "flutter_fc_media_event";
  // 事件通道
  private EventChannel eventChannel;
  private EventChannel.EventSink eventSink;
  // 消息传递器
  private BinaryMessenger binaryMessenger;


  @Override
  public void onAttachedToEngine(@NonNull FlutterPluginBinding flutterPluginBinding) {

    channel = new MethodChannel(flutterPluginBinding.getBinaryMessenger(), "flutter_fc_media");
    channel.setMethodCallHandler(this);

    new EventChannel(flutterPluginBinding.getBinaryMessenger(), eventChannelName).setStreamHandler(
            new EventChannel.StreamHandler() {
              @Override
              public void onListen(Object arguments, EventChannel.EventSink events) {
                Log.d("FlutterFcMediaPlugin", "已建立连接");
                eventSink = events;
              }
              @Override
              public void onCancel(Object arguments) {
                Log.d("FlutterFcMediaPlugin", "已断开连接");
              }
            }
    );

  }

  @Override
  public void onMethodCall(@NonNull MethodCall call, @NonNull Result result) {

    if (call.method.equals("getPlatformVersion")) {

      result.success("Android " + android.os.Build.VERSION.RELEASE);
    }  else if (call.method.equals("startListenEvent")) {
      Log.d("FlutterFcMediaPlugin", "准备注册");
      //String tmp = call.argument["rtp"];
      int rtpPort = call.argument("rtp");
      FCMediaEvent.handleMediaEventRTP(rtpPort, 8554, new FCMediaEvent.Callback() {
        @Override
        public void onResult(String rtspPath) {
          Log.d("FlutterFcMediaPlugin", "当前获取到的路径是: " + rtspPath);
          eventSink.success(rtspPath);
        }
      });
      result.success(false);
    } else if(call.method.equals("handleForMediaResetAll")){
      System.out.println("xxxxxxxxxxxxxxxxxxxxxx reset zlm xxxxxxxxxxxxxxxxxxxxxxxxxxxxxXxxXXх");
      FCMediaEvent.reset();
      result.success(false);
    }
    else {
      result.notImplemented();
    }
  }

  @Override
  public void onDetachedFromEngine(@NonNull FlutterPluginBinding binding) {
    channel.setMethodCallHandler(null);
    if(eventSink != null) {
      eventSink.endOfStream();
      eventSink = null;
    }
  }

}
